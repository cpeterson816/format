chrome.devtools.panels.elements.createSidebarPane("ForMat", 
    function(sidebar) {
        //sidebar initialization code here
        sidebar.setPage("forMat.html");
    });
//Above creates a sidebar. 
//To display content in a sidebar pane: 
//Call setPage to specify an HTML page to display in the pane. 
//Pass a JSON object to setObject
//Or Pass an expression to setExpression. DevTools evaluates the expression inthe context of the inspected page, and displays the return value. 


//  Messaging from Content Scripts to the DevTools Page:

/*
Messaging between the DevTools page and content scripts is indirect, by way of the background page. 

When sending a message to a content script, the background page can use the tabs.sendMessage method,
which directs a message to the content scripts in a specific tab, as shown in 'Injecting a Content Script'.

When sending a message from a content script, there is no ready-made method to deliver a message to the 
correct DevTools page instance associated with the current tab. As a workaround, you can have the DevTools
page establish a long-lived connection with the background page, and have the backgorund page keep a map
of tab IDs to connections, so it can route each message to the correct connection. 
*/
let connections = {};

chrome.runtime.onConnect.addListener(function (port) {

    let extensionListener = function (message, sender, sendResponse) {

        //The original connection event doesn't include the tab ID of the 
        //DevTools page, so we need to send it explicitly. 
        if (message.name == "init") {
            connections[message.tabId] = port;
            return;
        }

        //other message handling
    }

    //Listen to messages sent from the DevTools page
    port.onMessage.addListener(extensionListener);

    port.onDisconnect.addListener(function(port) {
        port.onMessage.removeListener(extensionListener);

        let tabs = Object.keys(connections);
        for (var i=0, len=tabs.length; i < len; i++) {
            if (connections[tabs[i]] == port) {
                delete connections[tabs[i]]
                break;
            }
        }
    });
});

//Receive message from content script and relay to the devTools page for the current tab
chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {
    //Messages from content scripts should have sender.tab set
    if (sender.tab) {
        let tabId = sender.tab.id;
        if (tabId in connections) {
            connections[tabId].postMessage(request);
        } else {
            console.log("Tab not found in connection list.");
        } 
    } else {
        console.log("sender.tab not defined.");
    }
    return true;
});