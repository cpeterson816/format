/*
                Passing the Selected Element to a Content Script:

The content script doesn't have direct access to the current selected element. However, any code you execute using inspectedWindow.eval has access to the DevTools
console and command-line APIs. For example, in evaluated code you can use $0 to access the selected element. 

To pass the selected element to a content script: 

-Create a method in the content script that takes the selected element as an argument. 
-Call the method from the DevTools page using inspectedWindow.eval with the useContentScriptContext: true option. 

The code in your content script might look something like this: 

function setSelectedElement(el) {
    //do something with the selected element
}

Invoke the method from the DevTools page like this: 

chrome.devtools.inspectedWindow.eval("setSelectedElement($0)",
    { useContentScriptContext: true });

The useContentScriptContext: true option specifies that the expression must be evaluated in the same context as the content scripts, so it can access
the setSelectedElement method. 
*/

chrome.devtools.panels.elements.createSidebarPane("ForMat",
    function(sidebar) {
        sidebar.setPage("forMat.html");
    });

onSelectionChanged.addListener(function() {
    //Change display to options based on selected element. 

});

